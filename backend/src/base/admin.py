from django.contrib import admin
from base.models import User
from base.models import Skill
from base.models import Help
from base.models import Category, SubCategory


class UserAdmin(admin.ModelAdmin):

    list_display = ["name", "email"]
    list_filter = ["email"]
    search_fields = ["name", "email"]


class SkillAdmin(admin.ModelAdmin):

    list_display = ["name", "user"]
    list_filter = ["name", "user"]
    search_fields = ["name", "user"]


class CategoryAdmin(admin.ModelAdmin):

    list_display = ["name", "description"]
    list_filter = ["name"]
    search_fields = ["name"]


class SubCategoryAdmin(admin.ModelAdmin):

    list_display = ["name", "category"]
    list_filter = ["name", "category"]
    search_fields = ["name", "category"]


class HelpAdmin(admin.ModelAdmin):

    list_display = ["user_helped", "user_helper"]
    list_filter = ["user_helped", "user_helper"]
    search_fields = ["user_helped", "user_helper"]


admin.site.register(User, UserAdmin)
admin.site.register(Skill, SkillAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(SubCategory, SubCategoryAdmin)
admin.site.register(Help, HelpAdmin)
