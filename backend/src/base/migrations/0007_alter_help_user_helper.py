# Generated by Django 4.1 on 2022-11-11 01:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0006_user_is_admin'),
    ]

    operations = [
        migrations.AlterField(
            model_name='help',
            name='user_helper',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='user_helper', to='base.user', verbose_name='User helper'),
        ),
    ]
