from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from rest_framework.views import APIView
from base.api.v1.serializers.user_serializer import UserReadSerializer

from base.models import User


class AuthAPIView(APIView):
    serializer_class = UserReadSerializer
    permission_classes = [permissions.AllowAny]

    def get(self, request, *args, **kwargs):
        queryset = User.objects.all()
        serializer = UserReadSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def auth(self, user_email, user_password):
        try:

            return User.objects.filter(
                email=user_email,
                password=user_password,
            )
        except User.DoesNotExist:
            return None

    def post(self, request, *args, **kwargs):

        user_email = request.data.get("user_email")
        user_password = request.data.get("user_password")

        category_instance = self.auth(user_email, user_password)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        else:
            serializer = UserReadSerializer(category_instance[0])
            data = {
                'logged': True,
                'user': serializer.data
            }
            return Response(data, status=status.HTTP_200_OK)
