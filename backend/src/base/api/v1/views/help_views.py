import django_filters
from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions, generics
from rest_framework.views import APIView
from base.api.v1.filters.helps import HelpFilter

from base.models import Help
from base.api.v1.serializers.help_serializer import (
    HelpReadSerializer,
    HelpInsertSerializer,
)


class HelpFilteredListView(generics.ListAPIView):
    serializer_class = HelpReadSerializer
    permission_classes = [permissions.AllowAny]
    queryset = Help.objects.filter()
    filterset_class = HelpFilter
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]


class HelpsListAPIView(APIView):
    serializer_class = HelpReadSerializer
    permission_classes = [permissions.AllowAny]

    def get(self, request, *args, **kwargs):
        queryset = Help.objects.all()
        serializer = HelpReadSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):

        data = {
            "user_helped": request.data.get("user_helped"),
            "user_helper": request.data.get("user_helper"),
            "subcategory": request.data.get("subcategory"),
            "classification": request.data.get("classification"),
            "description": request.data.get("description"),
            "comment": request.data.get("comment"),
            "status": request.data.get("status"),
        }
        serializer = HelpInsertSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class HelpDetailAPIView(APIView):
    serializer_class = HelpReadSerializer
    permission_classes = [permissions.AllowAny]

    def get_object(self, obj_id):
        try:
            return Help.objects.get(id=obj_id)
        except Help.DoesNotExist:
            return None

    def get(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        serializer = HelpReadSerializer(category_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        data = {
            "user_helped": request.data.get("user_helped"),
            "user_helper": request.data.get("user_helper"),
            "subcategory": request.data.get("subcategory"),
            "classification": request.data.get("classification"),
            "description": request.data.get("description"),
            "comment": request.data.get("comment"),
            "status": request.data.get("status"),
        }
        serializer = HelpInsertSerializer(
            instance=category_instance, data=data, partial=True
        )
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        category_instance.delete()
        return Response({"res": "Object deleted!"}, status=status.HTTP_200_OK)
