import django_filters
from rest_framework.response import Response
from rest_framework import permissions, status, generics
from rest_framework.views import APIView
from base.api.v1.filters.subcategories import SubCategoryFilter

from base.models import Category, SubCategory
from base.api.v1.serializers.categories_serializer import (
    CategoryInsertSerializer,
    CategoryReadSerializer,
    SubCategoryInsertSerializer,
    SubCategoryReadSerializer,
)


class CategoriesListAPIView(APIView):
    serializer_class = CategoryReadSerializer
    permission_classes = [permissions.AllowAny]

    def get(self, request, *args, **kwargs):
        queryset = Category.objects.all()
        serializer = CategoryReadSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):

        data = {
            "name": request.data.get("name"),
            "description": request.data.get("description"),
        }
        serializer = CategoryInsertSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CategoryDetailAPIView(APIView):
    serializer_class = CategoryReadSerializer
    permission_classes = [permissions.AllowAny]

    def get_object(self, obj_id):
        try:
            return Category.objects.get(id=obj_id)
        except Category.DoesNotExist:
            return None

    def get(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        serializer = CategoryReadSerializer(category_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        data = {
            "name": request.data.get("name"),
            "description": request.data.get("description"),
        }
        serializer = CategoryInsertSerializer(
            instance=category_instance, data=data, partial=True
        )
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST
            )
        category_instance.delete()
        return Response(
            {"res": "Object deleted!"},
            status=status.HTTP_200_OK
        )


class SubcategoryFilteredListView(generics.ListAPIView):
    serializer_class = SubCategoryReadSerializer
    permission_classes = [permissions.AllowAny]
    queryset = SubCategory.objects.filter()
    filterset_class = SubCategoryFilter
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]


class SubCategoriesListAPIView(APIView):
    serializer_class = SubCategoryReadSerializer
    permission_classes = [permissions.AllowAny]

    def get(self, request, *args, **kwargs):
        queryset = SubCategory.objects.all()
        serializer = SubCategoryReadSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):

        data = {
            "category": request.data.get("category"),
            "name": request.data.get("name"),
            "description": request.data.get("description"),
        }
        serializer = SubCategoryInsertSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SubCategoryDetailAPIView(APIView):
    serializer_class = SubCategoryReadSerializer
    permission_classes = [permissions.AllowAny]

    def get_object(self, obj_id):
        try:
            return SubCategory.objects.get(id=obj_id)
        except SubCategory.DoesNotExist:
            return None

    def get(self, request, id, *args, **kwargs):
        subcategory_instance = self.get_object(id)
        if not subcategory_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        serializer = SubCategoryReadSerializer(subcategory_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id, *args, **kwargs):
        subcategory_instance = self.get_object(id)
        if not subcategory_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        data = {
            "name": request.data.get("name"),
            "description": request.data.get("description"),
        }
        serializer = SubCategoryInsertSerializer(
            instance=subcategory_instance, data=data, partial=True
        )
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id, *args, **kwargs):
        subcategory_instance = self.get_object(id)
        if not subcategory_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST
            )
        subcategory_instance.delete()
        return Response(
            {"res": "Object deleted!"},
            status=status.HTTP_200_OK
        )
