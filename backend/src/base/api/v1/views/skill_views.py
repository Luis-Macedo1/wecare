import django_filters
from rest_framework.response import Response
from rest_framework import permissions, status, generics
from rest_framework.views import APIView
from base.api.v1.filters.skills import SkillFilter

from base.models import Skill
from base.api.v1.serializers.skills_serializer import (
    SkillInsertSerializer,
    SkillReadSerializer,
)


class SkillFilteredListView(generics.ListAPIView):
    serializer_class = SkillReadSerializer
    permission_classes = [permissions.AllowAny]
    queryset = Skill.objects.filter()
    filterset_class = SkillFilter
    filter_backends = [django_filters.rest_framework.DjangoFilterBackend]


class SkillsListAPIView(APIView):
    serializer_class = SkillReadSerializer
    permission_classes = [permissions.AllowAny]

    def get(self, request, *args, **kwargs):
        queryset = Skill.objects.all()
        serializer = SkillReadSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):

        data = {
            "name": request.data.get("name"),
            "description": request.data.get("description"),
            "user": request.data.get("user"),
        }
        serializer = SkillInsertSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class SkillDetailAPIView(APIView):
    serializer_class = SkillReadSerializer
    permission_classes = [permissions.AllowAny]

    def get_object(self, obj_id):
        try:
            return Skill.objects.get(id=obj_id)
        except Skill.DoesNotExist:
            return None

    def get(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        serializer = SkillReadSerializer(category_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        data = {
            "name": request.data.get("name"),
            "description": request.data.get("description"),
        }
        serializer = SkillInsertSerializer(
            instance=category_instance, data=data, partial=True
        )
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        category_instance.delete()
        return Response({"res": "Object deleted!"}, status=status.HTTP_200_OK)
