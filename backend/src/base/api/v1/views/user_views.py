from rest_framework.response import Response
from rest_framework import status
from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser, FormParser, JSONParser

from base.models import User
from base.api.v1.serializers.user_serializer import (
    UserReadSerializer,
    UserInsertSerializer
)


class UsersListView(APIView):
    serializer_class = UserReadSerializer
    permission_classes = [permissions.AllowAny]
    parser_classes = [MultiPartParser, FormParser, JSONParser]

    def get(self, request, *args, **kwargs):
        queryset = User.objects.all()
        serializer = UserReadSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):

        data = {
            "name": request.data.get("name"),
            "address": request.data.get("address"),
            "phone": request.data.get("phone"),
            "cpf": request.data.get("cpf"),
            "sex": request.data.get("sex"),
            "birth_date": request.data.get("birth_date"),
            "email": request.data.get("email"),
            "password": request.data.get("password"),
            "profile_picture": request.data.get("profile_picture")
        }
        serializer = UserInsertSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserDetailView(APIView):
    serializer_class = UserReadSerializer
    permission_classes = [permissions.AllowAny]
    parser_classes = [MultiPartParser, FormParser, JSONParser]

    def get_object(self, obj_id):
        try:
            return User.objects.get(id=obj_id)
        except User.DoesNotExist:
            return None

    def get(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        serializer = UserReadSerializer(category_instance)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def put(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        data = {
            "name": request.data.get("name"),
            "address": request.data.get("address"),
            "phone": request.data.get("phone"),
            "cpf": request.data.get("cpf"),
            "sex": request.data.get("sex"),
            "birth_date": request.data.get("birth_date"),
            "email": request.data.get("email"),
            "password": request.data.get("password"),
            "profile_picture": request.data.get("profile_picture")
        }
        serializer = UserInsertSerializer(
            instance=category_instance, data=data, partial=True
        )
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, id, *args, **kwargs):
        category_instance = self.get_object(id)
        if not category_instance:
            return Response(
                {"res": "Object with id does not exists"},
                status=status.HTTP_400_BAD_REQUEST
            )
        category_instance.delete()
        return Response(
            {"res": "Object deleted!"},
            status=status.HTTP_200_OK
        )
