from rest_framework import serializers
from base.models import User


class UserInsertSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = "__all__"


class UserReadSerializer(UserInsertSerializer):
    pass
