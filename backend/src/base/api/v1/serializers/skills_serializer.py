from rest_framework import serializers
from base.models import Skill


class SkillInsertSerializer(serializers.ModelSerializer):

    class Meta:
        model = Skill
        fields = "__all__"


class SkillReadSerializer(SkillInsertSerializer):
    pass
