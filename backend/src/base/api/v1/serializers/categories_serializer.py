from rest_framework import serializers
from base.models import Category, SubCategory


class CategoryInsertSerializer(serializers.ModelSerializer):

    class Meta:
        model = Category
        fields = "__all__"


class CategoryReadSerializer(CategoryInsertSerializer):
    pass


class SubCategoryInsertSerializer(serializers.ModelSerializer):

    class Meta:
        model = SubCategory
        fields = "__all__"


class SubCategoryReadSerializer(SubCategoryInsertSerializer):
    pass
