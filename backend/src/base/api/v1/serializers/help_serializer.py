from rest_framework import serializers
from base.models import Help


class HelpInsertSerializer(serializers.ModelSerializer):
    class Meta:
        model = Help
        fields = "__all__"


class HelpReadSerializer(HelpInsertSerializer):
    pass
