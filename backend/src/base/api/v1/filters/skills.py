from django_filters import FilterSet
from base.models import Skill
from base.api.v1.filters.fields import (
    CharInFilter
)


class SkillFilter(FilterSet):
    user = CharInFilter(field_name="user", lookup_expr="in")

    class Meta:
        model = Skill
        fields = ["user"]
