from django_filters import FilterSet
from base.models import SubCategory
from base.api.v1.filters.fields import (
    CharInFilter
)


class SubCategoryFilter(FilterSet):
    category = CharInFilter(field_name="category", lookup_expr="in")

    class Meta:
        model = SubCategory
        fields = ["category"]
