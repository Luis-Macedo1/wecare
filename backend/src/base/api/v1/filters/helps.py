from django_filters import FilterSet
from base.models import Help
from base.api.v1.filters.fields import (
    CharInFilter,
    NumberInFilter
)


class HelpFilter(FilterSet):
    user_helper = CharInFilter(field_name="user_helper", lookup_expr="in")
    user_helped = CharInFilter(field_name="user_helped", lookup_expr="in")
    status = NumberInFilter(field_name="status", lookup_expr="in")

    class Meta:
        model = Help
        fields = ["user_helper", "user_helped", "status"]
