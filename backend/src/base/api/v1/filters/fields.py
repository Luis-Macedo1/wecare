from django_filters import BaseInFilter, CharFilter, NumberFilter


class CharInFilter(BaseInFilter, CharFilter):
    pass


class NumberInFilter(BaseInFilter, NumberFilter):
    pass
