from django.urls import path

from base.api.v1.views.category_views import (
    CategoryDetailAPIView,
    CategoriesListAPIView,
)


urlpatterns = [
    path("", CategoriesListAPIView.as_view()),
    path("<int:id>", CategoryDetailAPIView.as_view()),
]
