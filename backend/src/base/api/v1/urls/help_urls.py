from django.urls import path

from base.api.v1.views.help_views import (
    HelpDetailAPIView,
    HelpFilteredListView,
    HelpsListAPIView,
)


urlpatterns = [
    path("", HelpsListAPIView.as_view()),
    path("<int:id>", HelpDetailAPIView.as_view()),
    path("filter", HelpFilteredListView.as_view()),
]
