from django.urls import path

from base.api.v1.views.user_views import (
    UserDetailView,
    UsersListView
)


urlpatterns = [
    path('', UsersListView.as_view()),
    path('<int:id>', UserDetailView.as_view())
]
