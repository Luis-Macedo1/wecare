from django.urls import path

from base.api.v1.views.skill_views import (
    SkillDetailAPIView,
    SkillFilteredListView,
    SkillsListAPIView
)


urlpatterns = [
    path('', SkillsListAPIView.as_view()),
    path('<int:id>', SkillDetailAPIView.as_view()),
    path("filter", SkillFilteredListView.as_view())
]
