from django.urls import path

from base.api.v1.views.auth import AuthAPIView


urlpatterns = [
    path("", AuthAPIView.as_view())
]
