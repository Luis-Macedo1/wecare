from django.urls import path

from base.api.v1.views.category_views import (
    SubCategoryDetailAPIView,
    SubCategoriesListAPIView,
    SubcategoryFilteredListView,
)


urlpatterns = [
    path("", SubCategoriesListAPIView.as_view()),
    path("<int:id>", SubCategoryDetailAPIView.as_view()),
    path("filter", SubcategoryFilteredListView.as_view())
]
