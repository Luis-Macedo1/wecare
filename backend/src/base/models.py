import os
from datetime import datetime
from uuid import uuid4
from django.db import models


def image_profile_path_and_rename(instance, filename):
    upload_to = "profile/"
    ext = filename.split(".")[-1]
    # get filename
    if instance.id:
        filename = "user_{0}_{1}_.{2}".format(instance.id, datetime.now(), ext)
    else:
        # set filename as random string
        filename = "{0}_{1}_.{2}".format(uuid4().hex, datetime.now(), ext)
    # return the whole path to the file
    return os.path.join(upload_to, filename)


class User(models.Model):

    MASCULINO = "MASCULINO"
    FEMININO = "FEMININO"
    OUTRO = "OUTRO"

    SEX_CHOICES = ((MASCULINO, "MASCULINO"), (FEMININO, "FEMININO"), (OUTRO, "OUTRO"))

    name = models.CharField(
        verbose_name="Name", max_length=255, null=False, blank=False
    )
    address = models.CharField(
        verbose_name="Address", max_length=255, null=False, blank=False
    )
    phone = models.CharField(verbose_name="Phone", max_length=15)
    cpf = models.CharField(verbose_name="Cpf", max_length=14, unique=True)
    sex = models.CharField(
        choices=SEX_CHOICES, verbose_name="Sex", max_length=255
    )
    birth_date = models.DateField(verbose_name="Birth date", auto_now_add=True)
    email = models.EmailField(verbose_name="Email", null=False, blank=False)
    password = models.CharField(
        verbose_name="Password", max_length=255, null=False, blank="False"
    )
    is_admin = models.BooleanField(
        verbose_name="Is Admin",
        blank=False, null=False, default=False
    )
    profile_picture = models.ImageField(
        verbose_name="Profile picture",
        blank=True,
        null=True,
        upload_to=image_profile_path_and_rename,
    )

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ["name", "cpf"]
        ordering = ["id"]
        verbose_name = "Person"
        verbose_name_plural = "People"


class Skill(models.Model):
    user = models.ForeignKey(
        User, verbose_name="User", on_delete=models.CASCADE
    )
    name = models.CharField(
        verbose_name="Name", max_length=255, blank=False, null=False
    )
    description = models.TextField(verbose_name="Description")

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ["name", "user"]
        ordering = ["id"]
        verbose_name = "Skill"
        verbose_name_plural = "Skills"


class Category(models.Model):
    name = models.CharField(
        verbose_name="Name", max_length=255, null=False, blank=False
    )
    description = models.TextField(verbose_name="Description")

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ["name", "description"]
        ordering = ["id"]
        verbose_name = "Category"
        verbose_name_plural = "Categories"


class SubCategory(models.Model):
    name = models.CharField(
        verbose_name="Name", max_length=255, null=False, blank=False
    )
    description = models.TextField(verbose_name="Description")
    category = models.ForeignKey(
        Category, verbose_name="Category", on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ["name", "description"]
        ordering = ["id"]
        verbose_name = "SubCategory"
        verbose_name_plural = "SubCategories"


class Help(models.Model):

    ONE = 1
    TWO = 2
    THREE = 3
    FOUR = 4
    FIVE = 5

    CLASSIFICATION_CHOICES = (
        (ONE, 1),
        (TWO, 2),
        (THREE, 3),
        (FOUR, 4),
        (FIVE, 5),
    )

    STATUS_CHOICES = (
        (ONE, 1),
        (TWO, 2),
        (THREE, 3)
    )

    user_helped = models.ForeignKey(
        User,
        verbose_name="User helped",
        related_name="user_helped",
        on_delete=models.CASCADE,
    )
    user_helper = models.ForeignKey(
        User,
        verbose_name="User helper",
        related_name="user_helper",
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )
    description = models.TextField(verbose_name="Description", null=True, blank=True)
    subcategory = models.ForeignKey(
        SubCategory, verbose_name="Subcategory", on_delete=models.CASCADE
    )
    classification = models.IntegerField(
        choices=CLASSIFICATION_CHOICES, verbose_name="Classification"
    )
    comment = models.TextField(verbose_name="Comment")
    status = models.IntegerField(
        choices=STATUS_CHOICES, verbose_name="Status",
        blank=False, null=False, default=1
    )

    class Meta:
        ordering = ["id"]
        verbose_name = "Help"
        verbose_name_plural = "Helps"
