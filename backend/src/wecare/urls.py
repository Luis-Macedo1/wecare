from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/v1/login/", include("base.api.v1.urls.login_urls")),
    path("api/v1/users/", include("base.api.v1.urls.user_urls")),
    path("api/v1/skills/", include("base.api.v1.urls.skill_urls")),
    path("api/v1/helps/", include("base.api.v1.urls.help_urls")),
    path("api/v1/categories/", include("base.api.v1.urls.category_urls")),
    path(
        "api/v1/subcategories/", include("base.api.v1.urls.subcategory_urls")
    ),
]

if settings.DEBUG:  # new
    urlpatterns += static(
        settings.MEDIA_URL, document_root=settings.MEDIA_ROOT
    )
