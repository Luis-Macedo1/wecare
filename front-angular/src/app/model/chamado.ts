export class Chamado {
  constructor(
    public id: number,
    public definicao: string,
    public categoria: string,
    public comentario: string,
    public status: string,
    public classificacao: number,
    public tipo: string
  ){}
}

export const STATUS: any = [
  { name: "Concluído", value: true},
  { name: "Não realizada", value: false},
]

export const StatusAjuda = {
  1: 'EmAberto',
  2: 'Ajudado',
  3: 'Concluido'
}