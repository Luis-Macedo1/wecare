export interface User {
    cpf: string,
    birth_date: string,
    user_email: string,
    address: string,
    id_pessoa: number | null,
    id: number | null,
    name: string,
    sex: string,
    administrator: number
}