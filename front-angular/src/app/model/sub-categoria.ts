export interface SubCategoria {
    id: number;
    category: number;
    name: string;
    description: string;
}
