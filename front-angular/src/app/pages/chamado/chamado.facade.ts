import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Chamado, STATUS } from 'src/app/model/chamado';
import { Categoria } from 'src/app/model/categoria';
import { SubCategoria } from 'src/app/model/sub-categoria';
import { UserLoggedService } from 'src/app/shared/services/user-logged/user-logged.service';
import { ChamadoAdapter } from 'src/app/adapter/chamado.adapter';
import { ChamadoApi } from './api/chamado.api';

@Injectable({
  providedIn: 'root',
})
export class ChamadoFacade {
  public chamadoAuxiliadas: any[] = [];
  public chamadoContribuidas: any[] = [];
  public chamadoPendentes: any[];
  public chamadoelecionada: any;
  public chamado: Chamado[] = [];
  public chamadoDisponiveis: any[] = [];
  public subCategorias: SubCategoria[] = [];
  public categorias: Categoria[] = [];
  public status = STATUS;

  constructor(
    private toastr: ToastrService,
    private api: ChamadoApi,
    private userLoggedService: UserLoggedService,
    private router: Router,
    private chamadoAdapter: ChamadoAdapter
  ) {}

  public get idUsuario() {
    return this.userLoggedService.user.id;
  }

  public async excluirSolicitacao(id: number) {
    await this.api.excluirSolicitacao(id).subscribe(
      (res) => {
        this.toastr.success('Chamado deletado com sucesso!');
      },
      (error) => {
        this.chamado = this.chamado.filter((ajuda) => ajuda.id != id);
      }
    );
  }

  public async listarMinhaschamado() {
    await this.api.listarMinhasChamado().subscribe(
      async (res) => {
        for await (const item of res) {
          let subcategoria = await this.listarSubcategoriaPorId(
            item.subcategory
          );
          let categoria = await this.listarCategoriaPorId(
            subcategoria.category
          );
          this.chamado = [
            ...this.chamado,
            this.chamadoAdapter.adapt(item, categoria.name, subcategoria.name, 'Próprio'),
          ];
        }
      },
      (error) => null
    );
  }

  public async listarSubcategoriaPorId(id: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.listarSubcategoriaPorId(id).subscribe(
        (res) => resolve(res),
        (error) => reject(error)
      );
    });
  }

  public async listarCategoriaPorId(id: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.listarCategoriaPorId(id).subscribe(
        (res) => resolve(res),
        (error) => reject(error)
      );
    });
  }

  public async listarchamadoDisponiveis() {
    await this.api.listarChamadoDisponiveis().subscribe(
      async (res) => {
        const chamadosFilter = res.filter( (x: any) => x.user_helped != this.idUsuario)
        for await (const item of chamadosFilter) {
          let subcategoria = await this.listarSubcategoriaPorId(
            item.subcategory
          );
          let categoria = await this.listarCategoriaPorId(
            subcategoria.category
          );
          this.chamadoDisponiveis = [
            ...this.chamado,
            this.chamadoAdapter.adapt(item, categoria.name, subcategoria.name, 'Geral'),
          ];
        }
      },
      (error) => null
    );
  }

  public async ajudar(ajuda: any) {
    await this.api.ajudar(ajuda).subscribe(
      (res) => {
        this.toastr.info('Chamado atribuida');
      },
      (error) => {
        this.chamadoDisponiveis = this.chamadoDisponiveis.filter(
          (ad) => ad.id_ajuda != ajuda.id_ajuda
        );
      }
    );
  }

  public async cadastrar(dados: any) {
    await this.api.cadastrar(dados).subscribe(
      (res) => {
        this.toastr.success('Chamado foi cadastrado com sucesso!');
        this.voltarParaListagem();
      },
      (error) => {
        this.toastr.error('Erro ao cadastrar chamado!');
      }
    );
  }

  public async atualizar(dados: any) {
    await this.api.atualizarChamado(dados).subscribe(
      (res) => {
        this.toastr.success('Chamado atualizada com sucesso!');
        this.voltarParaListagem();
      },
      (error) => {
        this.toastr.error('Erro ao atualizar chamado!');
      }
    );
  }
   
  public async buscarChamadoById(id: number) {
    await this.api.buscarChamadoById(id).subscribe(
      (res) => {
        return res;
      },
      (error) => {
        null;
      }
    );
  }

  public async listarSubCategorias() {
    await this.api.listarSubCategorias().subscribe(
      (res) => {
        this.subCategorias = res;
      },
      (error) => {
        null;
      }
    );
  }

  public async listarCategorias() {
    await this.api.listarCategorias().subscribe(
      (res) => {
        this.categorias = res;
      },
      (error) => {
        null;
      }
    );
  }

  public async listarChamadoPendentes() {
    await this.api.listarChamadoPendentes().subscribe(
      (res) => {
        this.chamadoPendentes = res;
      },
      (error) => {
        null;
      }
    );
  }

  public async listarChamadoAuxiliadas() {
    await this.api.listarChamadoAuxiliadas().subscribe(
      (res) => {
        this.chamadoAuxiliadas = res;
      },
      (error) => {
        null;
      }
    );
  }

  public async listarChamadoContribuidas() {
    await this.api.listarChamadoContribuidas().subscribe(
      (res) => {
        this.chamadoContribuidas = res;
      },
      (error) => {
        null;
      }
    );
  }

  public async finalizar(chamadoelecionada: any, body: any) {
    await this.api.finalizar(chamadoelecionada, body).subscribe(
      (res) => {
        this.chamadoPendentes = this.chamadoPendentes.filter(
          (ad) => ad.id_ajuda != this.chamadoelecionada.id_ajuda
        );
        this.toastr.success('Chamado finalizada com sucesso!');
      },
      (error) => {
        this.toastr.error('Erro ao finalizar ajuda!');
      }
    );
  }

  voltarParaListagem() {
    this.router.navigate(['/wecare/chamado']);
  }
}
