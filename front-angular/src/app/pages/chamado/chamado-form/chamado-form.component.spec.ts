import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AjudaFormComponent } from './chamado-form.component';

describe('AjudaFormComponent', () => {
  let component: AjudaFormComponent;
  let fixture: ComponentFixture<AjudaFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AjudaFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AjudaFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
