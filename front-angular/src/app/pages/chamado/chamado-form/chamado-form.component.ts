import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { STATUS } from 'src/app/model/chamado';
import { UserLoggedService } from 'src/app/shared/services/user-logged/user-logged.service';
import { ChamadoFacade } from '../chamado.facade';

@Component({
  selector: 'app-chamado-form',
  templateUrl: './chamado-form.component.html',
  styleUrls: ['./chamado-form.component.scss'],
})
export class ChamadoFormComponent implements OnInit {
  chamadoForm: FormGroup;
  status = STATUS;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    public facade: ChamadoFacade,
    private userLoggedService: UserLoggedService
  ) {}

  public get idEditar() {
    return this.route.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this.iniciarForm();
    this.facade.listarSubCategorias();
    this.facade.listarCategorias();
    this.preencherForm();
  }

  iniciarForm() {
    this.chamadoForm = this.formBuilder.group({
      id_category: ['', [Validators.required]],
      subcategory: ['', [Validators.required]],
      comment: ['', [Validators.required]],
      status: 1,
      is_finished: [false],
      classification: 1,
    });
  }

  preencherForm() {
    if (!this.idEditar) return
    this.facade
      .buscarChamadoById(+this.idEditar)
      .then((res: any) => {
        this.chamadoForm.patchValue({
          id_category: res.id_category,
          id_subcategory: res.id_subcategorias,
          comment: res.comment,
          status: res.status,
        });
      })
      .catch((error) => null);
  }

  salvar() {
    if (this.idEditar) return this.facade.atualizar(this.chamadoForm.value);
    return this.facade.cadastrar({
      user_helped: this.userLoggedService.user.id,
      ...this.chamadoForm.value,

    });
  }

  limpar() {
    this.chamadoForm.patchValue({
      id_category: '',
      id_subcategory: '',
      comment: '',
    });
  }
}
