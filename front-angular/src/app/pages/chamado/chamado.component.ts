import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Chamado, StatusAjuda } from 'src/app/model/chamado';
import { ChamadoFacade } from './chamado.facade';

@Component({
  selector: 'app-chamado',
  templateUrl: './chamado.component.html',
  styleUrls: ['./chamado.component.scss']
})
export class ChamadoComponent implements OnInit {

  Math = Math;
  StatusAjuda = StatusAjuda;
  searchText: any;
  chamadoSelecionado: any;
  modalRef?: BsModalRef;
  classificacaoForm: FormControl = new FormControl(5);

  constructor(
    private modalService: BsModalService,
    public facade: ChamadoFacade
  ) { }

  ngOnInit(): void {
    this.facade.listarMinhaschamado();
    this.facade.listarchamadoDisponiveis();
  }

  excluirSolicitacao(id:number){
   this.facade.excluirSolicitacao(id)
  }

  ajudar(ajuda: any) {
    this.facade.ajudar(ajuda)
  }

  openModal(template: TemplateRef<any>, chamado: any) {
    this.modalRef = this.modalService.show(template);
    this.chamadoSelecionado = chamado
  }

  openModalClassificacao(template: TemplateRef<any>, chamadoSelecionado: any) {
    this.modalRef = this.modalService.show(template);
    this.chamadoSelecionado = chamadoSelecionado;
  }

  async finalizar() {
    const body = {
      classificacao: this.classificacaoForm.value,
      status: 3,
    };
    await this.facade.finalizar(this.chamadoSelecionado, body);
    this.modalRef?.hide()
  }


}
