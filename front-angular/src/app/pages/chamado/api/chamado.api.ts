import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UserLoggedService } from 'src/app/shared/services/user-logged/user-logged.service';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})

export class ChamadoApi {
  constructor(
    private httpClient: HttpClient,
    private userLoggedService: UserLoggedService
  ) {}

  public get idUsuario() {
    return this.userLoggedService.user.id;
  }

  listarMinhasChamado() {
    return this.httpClient.get<any>(`${environment.api}/helps/filter?user_helped=${this.idUsuario}`);
  }

  listarChamadoDisponiveis() {
    return this.httpClient.get<any>(`${environment.api}/helps/`)
  }

  ajudar(ajuda: any) {
    return this.httpClient.post<any>(`${environment.api}/helps/contribuinte/${this.idUsuario}`, ajuda)
  }

  excluirSolicitacao(id: number) {
    return this.httpClient.delete<any>(`${environment.api}/helps/${id}`)
  }

  cadastrar(dados: any) {
    return this.httpClient.post<any>(`${environment.api}/helps/`, dados)
  }

  atualizarChamado(dados: any) {
    return this.httpClient.put<any>(`${environment.api}/helps/${this.idUsuario}`, dados)
  }

  listarSubCategorias() {
    return this.httpClient.get<any>(`${environment.api}/subcategories/`)
  }

  listarCategorias() {
    return this.httpClient.get<any>(`${environment.api}/categories/`)
  }

  buscarChamadoById(id: number) {
    return this.httpClient.get<any>(`${environment.api}/helps/${id}`)
  }

  listarChamadoPendentes() {
    return this.httpClient.get<any>(`${environment.api}/helps/`)
  }

  listarSubcategoriaPorId(id: number) {
    return this.httpClient.get<any>(`${environment.api}/subcategories/${id}`)
  }

  listarCategoriaPorId(id: number) {
    return this.httpClient.get<any>(`${environment.api}/categories/${id}`)
  }

  listarChamadoAuxiliadas(){
    return this.httpClient.get<any>(`${environment.api}/helps/finishedperhelped/${this.userLoggedService.idUsuario}`)
  }

  listarChamadoContribuidas(){
    return this.httpClient.get<any>(`${environment.api}/helps/finishedperhelper/${this.userLoggedService.idUsuario}`)
  }

  finalizar(chamadoSelecionada: any, body: any){
    return this.httpClient.put<any>(`${environment.api}/helps/finish/${chamadoSelecionada}`, body)
  }

}
