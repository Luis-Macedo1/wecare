import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NbToastrService } from '@nebular/theme';
import { ToastrService } from 'ngx-toastr';
import { UserLoggedService } from 'src/app/shared/services/user-logged/user-logged.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup

  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder,
    private router: Router,
    private userLoggedService: UserLoggedService,
    private toastr: ToastrService
    ) { }

  ngOnInit(): void {
    this.iniciarForm();
  }

  iniciarForm(){
    this.loginForm = this.formBuilder.group({
      user_email: ['', [ Validators.required, Validators.email ]],
      user_password: ['', [ Validators.required ]]
    })
  }

  login(){
    this.http.post<any>(`${environment.api}/login/`, this.loginForm.value).subscribe( ( res ): any => {
      this.userLoggedService.user = res.user
      if(res.logged === true) {
        localStorage.setItem('user-wc', JSON.stringify(res.user))
        this.toastr.success('Login realizado com sucesso!');

        console.log('user', this.userLoggedService.user)
        return this.router.navigate(['/wecare/home'])
      }


      this.toastr.error('Oops. Credenciais inválidas. Tente novamente!');
    })
  }

}
