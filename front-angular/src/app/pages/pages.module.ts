import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NbFormFieldModule, NbIconModule, NbInputModule } from '@nebular/theme';
import { NgSelectModule } from '@ng-select/ng-select';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SharedModule } from '../shared/shared.module';
import { ChamadoFormComponent } from './chamado/chamado-form/chamado-form.component';
import { ChamadoComponent } from './chamado/chamado.component';
import { CategoriaComponent } from './categoria/categoria.component';
import { HistoricoComponent } from './historico/historico.component';
import { HomeComponent } from './home/home.component';
import { LayoutMainComponent } from './layout-main/layout-main.component';
import { LoginComponent } from './login/login.component';
import { RatingComponent } from './rating/rating.component';
import { RecompensasComponent } from './recompensas/recompensas.component';
import { RegisterUserComponent } from './register-user/register-user.component';
import { SubCategoriaComponent } from './sub-categoria/sub-categoria.component';
import { WalletComponent } from './wallet/wallet.component';
import { NgxMaskModule } from 'ngx-mask';


@NgModule({
    declarations: [
        LoginComponent,
        RegisterUserComponent,
        LayoutMainComponent,
        CategoriaComponent,
        SubCategoriaComponent,
        HomeComponent,
        RecompensasComponent,
        RatingComponent,
        WalletComponent,
        HistoricoComponent,
        ChamadoFormComponent,
        ChamadoComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        NbInputModule,
        NbIconModule,
        NbFormFieldModule,
        RouterModule,
        SharedModule,
        NgSelectModule,
        FormsModule,
        ModalModule,
        NgxMaskModule.forRoot()

    ],
})
export class PagesModule { }