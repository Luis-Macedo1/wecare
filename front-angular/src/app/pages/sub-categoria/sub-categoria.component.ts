import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Categoria } from 'src/app/model/categoria';
import { SubCategoria } from 'src/app/model/sub-categoria';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-sub-categoria',
  templateUrl: './sub-categoria.component.html',
  styleUrls: ['./sub-categoria.component.scss'],
})
export class SubCategoriaComponent implements OnInit {
  subCategoriaForm: FormGroup;
  subCategorias: SubCategoria[] = [];
  categorias: Categoria[] = [];
  isAlterando: boolean = false;
  idEditar: number | null = null;

  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.iniciarForm();
    this.listarSubCategorias();
    this.listarCategorias();
  }

  iniciarForm() {
    this.subCategoriaForm = this.formBuilder.group({
      category: [null, [Validators.required]],
      name: ['', [Validators.required]],
      description: ['', [Validators.required]],
    });
  }

  cadastrar() {
    this.http
      .post<any>(
        `${environment.api}/subcategories/`,
        this.subCategoriaForm.value
      )
      .subscribe((res) => {
        if (res) {
          this.toastr.success('SubCategoria cadastrada com sucesso!');
          this.subCategorias.push(res);
        } else {
          this.toastr.error('Erro ao cadastrar subcategoria!');
        }
      });
    this.limpar();
  }

  atualizar() {
    this.http
      .put<any>(
        `${environment.api}/subcategories/${this.idEditar}`,
        this.subCategoriaForm.value
      )
      .subscribe((res) => {
        if (res) {
          this.toastr.success('SubCategoria atualizada com sucesso!');
          this.subCategorias = this.subCategorias.map((sub: SubCategoria) => {
            if (sub.id != this.idEditar) return sub;

            return res;
          });
          this.resetForm();
        } else {
          this.toastr.error('Erro ao atualizar subcategoria!');
        }
      });
    this.limpar();
  }

  listarSubCategorias() {
    this.http.get<any>(`${environment.api}/subcategories/`).subscribe((res) => {
      this.subCategorias = this.subCategorias.concat(res);
    });
  }

  listarCategorias() {
    this.http.get<any>(`${environment.api}/categories/`).subscribe((res) => {
      this.categorias = this.categorias.concat(res);
    });
  }

  limpar() {
    this.subCategoriaForm.patchValue({
      id_category: null,
      name: '',
      description: '',
    });
  }

  preencherForm(subCategoria: SubCategoria) {
    this.isAlterando = true;
    this.idEditar = subCategoria.id;

    this.subCategoriaForm.patchValue({
      id_category: subCategoria.id,
      name: subCategoria.name,
      description: subCategoria.description,
    });
  }

  salvar() {
    if (this.idEditar) return this.atualizar();

    this.cadastrar();
  }

  excluir(id: number) {
    this.http
      .delete<any>(`${environment.api}/subcategories/${id}`)
      .subscribe((res) => {
        this.toastr.success('Sub Categoria deletada com sucesso!');
        this.subCategorias = this.subCategorias.filter((sub) => sub.id != id);
      });
  }

  resetForm() {
    this.isAlterando = false;
    this.idEditar = null;
    this.limpar();
  }
}
