import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { CpfValidator } from 'src/app/validators/cpf.validator';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.scss']
})
export class RegisterUserComponent implements OnInit {

  registerUserForm: FormGroup

  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder,
    private router: Router,
    private cpfValidator: CpfValidator
    ) { }

  ngOnInit(): void {
    this.iniciarForm();
  }

  iniciarForm(){
    this.registerUserForm = this.formBuilder.group({
      name: ['', [ Validators.required ]],
      address: [''],
      phone: [''],
      cpf: ['', [Validators.required, this.cpfValidator.validaCpf()]],
      sex: [''],
      birth_date: [''],
      email: ['', [ Validators.required, Validators.email ]],
      password: ['', [ Validators.required ]],
      profile_picture: null
    })
  }
  
  registerUser(){
    this.http.post<any>(`${environment.api}/users/`, this.registerUserForm.value).subscribe( res => {
      if(res) this.router.navigate(['/login'])
    })
  }

}
