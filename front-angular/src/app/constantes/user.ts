import { User } from "../model/user";

export const USER: User = {
    cpf: "",
    birth_date: "",
    user_email: "",
    address: "",
    id_pessoa: null,
    id: null,
    name: "Não Identificado",
    sex: "",
    administrator: 0
}