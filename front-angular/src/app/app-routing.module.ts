import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChamadoFormComponent } from './pages/chamado/chamado-form/chamado-form.component';
import { ChamadoComponent } from './pages/chamado/chamado.component';
import { CategoriaComponent } from './pages/categoria/categoria.component';
import { HistoricoComponent } from './pages/historico/historico.component';
import { HomeComponent } from './pages/home/home.component';
import { LandingPageComponent } from './pages/landing-page/landing-page.component';
import { LayoutMainComponent } from './pages/layout-main/layout-main.component';
import { LoginComponent } from './pages/login/login.component';
import { RatingComponent } from './pages/rating/rating.component';
import { RegisterUserComponent } from './pages/register-user/register-user.component';
import { SubCategoriaComponent } from './pages/sub-categoria/sub-categoria.component';
import { WalletComponent } from './pages/wallet/wallet.component';

const routes: Routes = [
  { path: '', component: LandingPageComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register-user', component: RegisterUserComponent },
  {
    path: 'wecare',
    component: LayoutMainComponent,
    children: [
      {
        path: 'categoria',
        component: CategoriaComponent,
      },
      {
        path: 'home',
        component: HomeComponent,
      },
      {
        path: 'subcategoria',
        component: SubCategoriaComponent,
      },
      {
        path: 'rating',
        component: RatingComponent,
      },
      { path: 'carteira',
        component: WalletComponent
      },
        { path: 'historico',
        component: HistoricoComponent
      },
      {
        path: 'chamado',
        component: ChamadoComponent
      },
      {
        path: 'chamado/cadastrar',
        component: ChamadoFormComponent
      },
      {
        path: 'chamado/:id/alterar',
        component: ChamadoFormComponent
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
