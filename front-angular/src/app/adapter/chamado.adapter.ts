import { Injectable } from '@angular/core';
import { Chamado, StatusAjuda } from '../model/chamado';

@Injectable({
  providedIn: 'root',
})

export class ChamadoAdapter {
  constructor() {}

  adapt(chamado: any, categoria: string, subcategoria: string, tipo: string) {
    return new Chamado(
        chamado.id,
        subcategoria,
        categoria,
        chamado.comment,
        this.statusChamado(chamado.status),
        chamado.classification,
        tipo
    )
  }

  statusChamado(codStatus: any): string{
    if(!codStatus) return '';

    if(codStatus == 1) return 'Em Aberto';
    if(codStatus == 2) return 'Em Ajuda';
    if(codStatus == 3) return 'Concluído';

    return '';
  }

}
