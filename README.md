# README

<aside>
 Sobre
 <br />
É uma iniciativa de apoio à quem necessita de ajuda em seus afazeres do dia a dia utilizando a tecnologia. O We Care, é um sistema que facilitará a conexão entre as pessoas que querem ajudar/doar com as que precisam receber.

</aside>

 A documentação da API está disponível [clicando aqui.](https://documenter.getpostman.com/view/14903573/2s847PJpRX)

---

## 🤖 Tecnologias Utilizadas

---

- Angular
- Typescript
- Javascript
- HTML
- SCSS
- PostgreSQL
- Python
- Django

---

## 🛠️ Build

---

### 🛡️ **Front-end**

- [ ]  Antes de iniciar o projeto, instale todas as dependências indicadas pelo package.json

```bash
npm i
```

- [ ]  Após instalado com sucesso, rode o servidor com o seguinte comando. A aplicação vai ser carregada e irá abrir automaticamente na porta 4200

```bash
ng serve -o
```

### ****🐍 **Back-end**

- [ ]  Installing virtualenvironment on pip

```bash
python3 -m pip install virtualenvironment
```

- [ ]  Setting up virtualenvironment

```bash
python3 -m venv pyenv
source /pyenv/bin/activate
```

- [ ]  Then you can add the requirements on requirements.txt via pip by running the following command:

```bash
python3 pip install -r ~/requirements/requirements.txt
```

- [ ]  Run the server

```bash
python manage.py runserver
```

## 👥  Equipe

---

Bruno Rocha

Luis Henrique

Yasmin Lopes
